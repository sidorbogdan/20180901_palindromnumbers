public class PalindromeNumbers {
    public static void main(String[] args) {
        int n=11;
        int initialN=n;
        int reverseN=0;
        while (n>0) {
            int r = n % 10;
            n = n / 10;
            reverseN = reverseN * 10 + r;
        }if(initialN==reverseN) {
            System.out.println("number is palindrome");
        }else{
            System.out.println("number is not palindrome");
        }
    }
}
